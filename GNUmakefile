# NOTE: just for Proxmox packaging and called GNUmakefile to still allow direct (non-packaging) builds

include /usr/share/dpkg/default.mk

PACKAGE=swtpm

BUILDDIR ?= $(PACKAGE)-$(DEB_VERSION)
ORIG_SRC_TAR=$(PACKAGE)_$(DEB_VERSION_UPSTREAM).orig.tar.gz

GITVERSION:=$(shell git rev-parse HEAD)

DSC = $(PACKAGE)_$(DEB_VERSION).dsc
DEB = $(PACKAGE)_$(DEB_VERSION)_$(DEB_HOST_ARCH).deb
DEB_DBG = $(PACKAGE)-dbgsym_$(DEB_VERSION)_$(DEB_HOST_ARCH).deb
DEB_LIB = swtpm-libs_$(DEB_VERSION)_$(DEB_HOST_ARCH).deb
DEB_LIB_DBG = swtpm-libs-dbgsym_$(DEB_VERSION)_$(DEB_HOST_ARCH).deb
DEB_DEV = swtpm-dev_$(DEB_VERSION)_$(DEB_HOST_ARCH).deb
DEB_TOOLS = swtpm-tools_$(DEB_VERSION)_$(DEB_HOST_ARCH).deb
DEB_TOOLS_DBG = swtpm-tools-dbgsym_$(DEB_VERSION)_$(DEB_HOST_ARCH).deb

DEBS=$(DEB) $(DEB_DBG) $(DEB_LIB) $(DEB_LIB_DBG) $(DEB_DEV) $(DEB_TOOLS) $(DEB_TOOLS_DBG)

all:

$(BUILDDIR):
	rm -rf $@ $@.tmp
	rsync -a * $@.tmp
	rm -f $@.tmp/GNUmakefile # just for Proxmox packaging convenience
	echo "git clone git://git.proxmox.com/git/lxcfs.git\\ngit checkout $(GITVERSION)" > $@.tmp/debian/SOURCE
	mv $@.tmp $@

.PHONY: deb
deb: $(DEBS)
$(DEB_DBG) $(DEB_LIB) $(DEB_LIB_DBG) $(DEB_DEV) $(DEB_TOOLS) $(DEB_TOOLS_DBG): $(DEB)
$(DEB): $(BUILDDIR)
	cd $(BUILDDIR); dpkg-buildpackage -rfakeroot -b -us -uc
	lintian $(DEBS)

sbuild: $(DSC)
	sbuild $(DSC)

$(ORIG_SRC_TAR): $(BUILDDIR)
	tar czf $(ORIG_SRC_TAR) --exclude="$(BUILDDIR)/debian" $(BUILDDIR)

.PHONY: dsc
dsc:
	$(MAKE) clean
	$(MAKE) $(DSC)
	lintian $(DSC)

$(DSC): $(ORIG_SRC_TAR) $(BUILDDIR)
	cd $(BUILDDIR); dpkg-buildpackage -S -us -uc -d

.PHONY: upload
upload: UPLOAD_DIST ?= $(DEB_DISTRIBUTION)
upload: $(DEBS)
	tar cf - $(DEBS) | ssh repoman@repo.proxmox.com upload --product pve --dist $(UPLOAD_DIST)

.PHONY: clean
clean:
	rm -rf $(PACKAGE)-[0-9]*/ $(PACKAGE)*.tar* *.deb *.dsc *.changes *.buildinfo *.build
